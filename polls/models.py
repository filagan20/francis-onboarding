# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.utils import timezone

from django.db import models

# Create your models here.
class Question(models.Model):
	question_text = models.CharField(max_length=200)
	pub_date = models.DateTimeField("date published")
	def __str__(self):
		return self.question_text;
	def is_recent(self):
		sDate = timezone.now() - datetime.timedelta(days=1)
		return sDate <= self.pub_date <= timezone.now()
	is_recent.admin_order_field = "pub_date"
	is_recent.boolean = True
	is_recent.short_description = "Is Published Recently?"


class Choice(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)
	def __str__(self):
		return self.choice_text;