# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils import timezone
from django.views import generic
from django.shortcuts import render, get_object_or_404

from django.urls import reverse

from django.http import HttpResponse, Http404, HttpResponseRedirect

from .models import Question, Choice

class IndexView(generic.ListView):
	template_name = "polls/index.html"
	context_object_name = "latest_questions"
	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now()).order_by("-pub_date")[:5]

class DetailView(generic.DetailView):
	model = Question
	template_name = "polls/detail.html"
	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now())

class ResultView(generic.DetailView):
	model = Question
	template_name = "polls/result.html"	
	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now())

def vote(request, question_id):
	question = get_object_or_404(Question, pk=question_id)
	try:
		sChoice = question.choice_set.get(pk=request.POST["qChoice"])
	except(KeyError, Choice.DoesNotExist):
		return render(request, "polls/detail.html", {
				"question": question,
				"error_message": "You have selected nothing."
			})
	else:
		sChoice.votes += 1
		sChoice.save()
		return HttpResponseRedirect(reverse("polls:results", args=(question.id,)))