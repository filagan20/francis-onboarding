# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Question,Choice

# Register your models here.
class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 3

class QuestionAdmin(admin.ModelAdmin):
	list_display = ("question_text", "pub_date", "is_recent")
	list_filter = ["pub_date"]
	search_fields = ["question_text"]
	inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)