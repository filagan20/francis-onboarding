import datetime

from django.test import TestCase

from django.utils import timezone
from django.urls import reverse

from .models import Choice, Question

# Create your tests here.

def create_question(qText, dOffset):
	"""
		Create question with question_text = qText and pub_date = now - dOffset (positive for future, negative for past)
	"""
	t = timezone.now() + datetime.timedelta(days = dOffset)
	return Question.objects.create(question_text = qText, pub_date = t)

#Testing Models
class QModelTests(TestCase):

	def test_future_question_is_recent(self):
		"""
			Future Questions should not be recent
		"""
		fq = create_question(qText = "Future Question", dOffset = 30)
		self.assertIs(fq.is_recent(), False)

	def test_old_past_question_is_recent(self):
		"""
			Questions older than 1 day should not be recent
		"""
		oq = create_question(qText = "Older Question", dOffset = -30)
		self.assertIs(oq.is_recent(), False)

	def test_recent_question_is_recent(self):
		"""
			Questions recently published under 1 day should be recent
		"""
		t = timezone.now() - datetime.timedelta(hours=23,minutes=59,seconds=59)
		rq = Question.objects.create(question_text="Recent Question", pub_date=t)
		self.assertIs(rq.is_recent(), True)

#Test Views
class QIndexViewTests(TestCase):

	def test_no_questions(self):
		"""
			A message should appear in case of no question found
		"""
		response = self.client.get(reverse("polls:index"))
		self.assertEqual(response.status_code, 200)
		self.assertContains(response, "No questions found.")
		self.assertQuerysetEqual(
			response.context["latest_questions"],
			[]
		)

	def test_past_question_only(self):
		"""
			Past questions should be displayed
		"""
		create_question(qText = "Past Question", dOffset = -30)
		response = self.client.get(reverse("polls:index"))
		self.assertQuerysetEqual(
			response.context["latest_questions"], 
			['<Question: Past Question>']
		)

	def test_future_question_only(self):
		"""
			Future questions should not be displayed
		"""
		create_question(qText = "Future Question", dOffset = 30)
		response = self.client.get(reverse("polls:index"))
		self.assertQuerysetEqual(
			response.context["latest_questions"],
			[]
		)

	def test_past_and_future_question(self):
		"""
			Only past question will be displayed
		"""
		create_question(qText = "Past Question", dOffset = -30)
		create_question(qText = "Future Question", dOffset = 30)
		response = self.client.get(reverse("polls:index"))
		self.assertQuerysetEqual(
			response.context["latest_questions"],
			['<Question: Past Question>']
		)

	def test_multiple_past_questions(self):
		"""
			Multiple past questions can be displayed
		"""
		create_question(qText = "Past 1", dOffset = -30)
		create_question(qText = "Past 2", dOffset = -5)
		response = self.client.get(reverse("polls:index"))
		self.assertQuerysetEqual(
			response.context["latest_questions"],
			['<Question: Past 2>', '<Question: Past 1>']
		)

class QDetailsViewTests(TestCase):

	def test_future_question(self):
		"""
			Future question details should not be accessed
		"""
		fQuestion = create_question(qText = "Future Question", dOffset = 30)
		url = reverse("polls:detail", args=(fQuestion.id,))
		response = self.client.get(url)
		self.assertEqual(response.status_code, 404)

	def test_past_question(self):
		"""
			Published question details can be accessed
		"""
		pQuestion = create_question(qText = "Past Question", dOffset = -30)
		url = reverse("polls:detail", args=(pQuestion.id,))
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertContains(response, pQuestion.question_text)

class QResultsViewTests(TestCase):

	def test_future_question(self):
		"""
			Future question results should not be accessed
		"""
		fQuestion = create_question(qText = "Future Question", dOffset = 30)
		url = reverse("polls:results", args=(fQuestion.id,))
		response = self.client.get(url)
		self.assertEqual(response.status_code, 404)

	def test_past_question(self):
		"""
			Published question results can be accessed
		"""
		pQuestion = create_question(qText = "Past Question", dOffset = -30)
		url = reverse("polls:results", args=(pQuestion.id,))
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertContains(response, pQuestion.question_text)
		self.assertQuerysetEqual(
			response.context["question"].choice_set.all(),
			pQuestion.choice_set.all()
		)